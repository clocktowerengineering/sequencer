import PyTango
from datetime import datetime
import json
import traceback

class SequenceValidator():

    
    def __init__(self):


        pass

    '''
        Sequence validator. Validates the following:
        
        Error codes:
        -1000 Is the string valid JSON?
        -1001 Is the type defined correctly?
        -1002 Are the keys for this type defined correctly, and are the values correct?
        
        '''
    
    def ValidateSequence(self,inputstring):
        
        # first step: validate that the input string is valid json. return -1000 if not and set status str.
        
        try : 
            
            jsonintermediate = json.loads(inputstring)
            
        except Exception as e:
            
            print(str(datetime.now()) + ": " + str(e))
            self.attr_statusText_read = "Improperly formatted JSON: " + str(e)
            traceback.print_exc()
            return -1000
            
        # second step: validate that each step has a type and is defined correctly.
        
        try:
            
            for stepnumber in range(1,len(jsonintermediate)+1):

                # check that the type meets the above format 
                if (jsonintermediate[str(int(stepnumber))]["type"]=="criteria") or (jsonintermediate[str(int(stepnumber))]["type"]=="parameter") or (jsonintermediate[str(int(stepnumber))]["type"]=="duration") or (jsonintermediate[str(int(stepnumber))]["type"]=="jump") or (jsonintermediate[str(int(stepnumber))]["type"]=="jumpIf") or (jsonintermediate[str(int(stepnumber))]["type"]=="jumpCompare"):
          
                    pass
                    
                else:
                    raise Exception
                    
            
        except Exception as e:
            
            print(str(datetime.now()) + ": " + "No or bad value at: " + str(int(stepnumber)) + "; " + str(e))
            self.attr_statusText_read = "No or bad value at: " + str(int(stepnumber)) + "; " + str(e)
            traceback.print_exc()
            return -1001
            
        # each step has a type. validate each step for correctness within that specific type.    
        
        try: 
            self.json_intermediate_length = int(len(jsonintermediate))
            for stepnumber in range(1,len(jsonintermediate)+1):
                
                if jsonintermediate[str(int(stepnumber))]["type"]=="criteria":
                    self.ValidateCriteria(jsonintermediate[str(int(stepnumber))])
                    
                elif jsonintermediate[str(int(stepnumber))]["type"]=="parameter":
                    self.ValidateParameter(jsonintermediate[str(int(stepnumber))])
                    
                elif jsonintermediate[str(int(stepnumber))]["type"]=="duration":
                    self.ValidateDuration(jsonintermediate[str(int(stepnumber))])
                    
                elif jsonintermediate[str(int(stepnumber))]["type"]=="jump":
                    self.ValidateJump(jsonintermediate[str(int(stepnumber))])
                    
                elif jsonintermediate[str(int(stepnumber))]["type"]=="jumpIf":
                    self.ValidateJumpIf(jsonintermediate[str(int(stepnumber))])
                elif jsonintermediate[str(int(stepnumber))]["type"]=="jumpCompare":
                    self.ValidateJumpCompare(jsonintermediate[str(int(stepnumber))])   
                    
                else:
                    raise Exception
                
            
            self.loaded_sequence = jsonintermediate
            self.attr_statusText_read = "Loaded!"
            return 1
                    
        except Exception as e:
            
            print(str(datetime.now()) + ": " + "No or bad key at: " + str(int(stepnumber)) + "; " + str(e))
            self.attr_statusText_read = "No or bad key at: " + str(int(stepnumber)) + "; " + str(e)
            traceback.print_exc()
            return -1002
            
    def ValidateCriteria(self,value):
        
        """
        "address":<TANGO device address>,
        "attribute" : "<TANGO attribute>",
        "value" : "<TANGO attribute value>"
        "criteriaType" : "lessThan" or "greaterThan" or "equals" or "notEquals"
        """
      
        self.ValidateTANGO(value)
            
        # check that there is a criteria type and one is properly selected. throw exception if key doesn"t exist, throw exception if key exists but bad type.
        if value["criteriaType"] == "lessThan" or value["criteriaType"] == "greaterThan" or value["criteriaType"] == "equals" or value["criteriaType"] == "notEquals":
            pass
        else:
            raise Exception
                
    def ValidateParameter(self,value):
        """
        if a parameter type (updates a parameter)
                "address":<TANGO device address>,
                "attribute" : "<TANGO attribute>",
                "value" : "<TANGO attribute value>"
        """
        
        self.ValidateTANGO(value)
        
    def ValidateDuration(self,value):
        """
        if a duration type (waits for some time in seconds)
                "duration" : <duration in seconds>
                
                a 0 second duration will immediately process as fast as the system can update.
            
        """
        # can"t have negative durations. Also, check that duration is a key in the dict.
        
        if int(value[str("duration")]) >= 0:
            pass
        else:
            raise Exception
            
    def ValidateJump(self,value):
        
        """
        if a jump type (jumps to a sequence step number)
                "jumpTo" : step to which to jump
        """        
        # check that the jump value is between 1 and the length of the sequence.
        if int(value["jumpTo"]) >=1 and int(value["jumpTo"])<= self.json_intermediate_length:
            pass
        else:
            print("bad step number to jump to!")
            raise Exception
        
    def ValidateJumpIf(self,value):    
        """
                if a jumpif type (jumps to a sequence step number if true at the time of execution)
                "address":<TANGO device address>,
                "attribute" : "<TANGO attribute>",,
                "value" : "<TANGO attribute value>"
                "criteriaType" : "lessThan" or "greaterThan" or "equals" or "notEquals"
                "jumpTo" : step to which to jump if true
        """
        self.ValidateCriteria(value)
        self.ValidateJump(value)
        
    def ValidateJumpCompare(self,value):
        """
                if a jumpCompare type (jumps to a sequence step number comparison to another attribute is true at the time of execution)
                "address":<TANGO device address>,
                "attribute" : "<TANGO attribute>",
                
                "criteriaType" : "lessThan" or "greaterThan" or "equals" or "notEquals"
                "jumpTo" : step to which to jump if true
                "compareaddress":<TANGO device address>,
                "compareattribute" : "<TANGO attribute>",
        """
        self.ValidateJumpIf(value)
        self.ValidateCompare(value)
        
        
    def ValidateCompare(self,value):
        
        # Validates the tango address, attribute, and value.
                # check that the address is a valid TANGO address.
        if value["compareaddress"].count("/")==2:
            pass
        else:
            print("Bad TANGO compareaddress.")
            raise Exception
            
        # check that there is a tango attribute. Do a fake check and throw exception if the key doesn"t exist. If a jumpCompare, move on.
        if value["type"] != "jumpCompare" :
            if value["compareattribute"] == "fakeCheck":
                pass
            

            
            
        # ok, now check that the address, attribute exist For real. If this call fails, it means something is hosed with the sequence or server.
        typestorage = PyTango.DeviceProxy(value["address"]).read_attribute(value["attribute"]).type
        typecomparestorage = PyTango.DeviceProxy(value["compareaddress"]).read_attribute(value["compareattribute"]).type
        # typestorage has the integer value of the PyTango TANGO device attribute. Check that the type is consistent with what's in the sequence.
        
        # only allow identical compare types.
        if (typestorage != typecomparestorage):
            
            print(str(value) +" attribute doesn't match the type of the compare attribute!")
            raise Exception
    
    def ValidateTANGO(self,value):
        
        # Validates the tango address, attribute, and value.
                # check that the address is a valid TANGO address.
        if value["address"].count("/")==2:
            pass
        else:
            print("Bad TANGO address.")
            raise Exception
            
        # check that there is a tango attribute. Do a fake check and throw exception if the key doesn"t exist.
        if value["attribute"] == "fakeCheck":
            pass
            
        # check that there is a tango attribute. Do a fake check and throw exception if the key doesn"t exist. If a jumpCompare, move on.
        if value["type"] != "jumpCompare" :
            if value["compareattribute"] == "fakeCheck":
                pass
            
            
        # ok, now check that the address, attribute exist For real. If this call fails, it means something is hosed with the sequence or server.
        typestorage = PyTango.DeviceProxy(value["address"]).read_attribute(value["attribute"]).type
        
        # typestorage has the integer value of the PyTango TANGO device attribute. Check that the type is consistent with what's in the sequence.
        
        # bool = 1
        # long = 3
        # float = 4
        # string = 8
        
        # only allow longs in a long.
        if (int(typestorage)==3):
            
            if isinstance(value["value"],int):
                pass
            else:
                print(str(value) +" attribute expected a long, but not a long!")
        
        # allow longs or floats in a float.
        elif (int(typestorage)==4):
            
            if isinstance(value["value"],int) or isinstance(value["value"],float):
                pass
            else:
                print(str(value) +" attribute expected a float, but not a long or float!")      
    
        # only allow bools in bool.
        elif (int(typestorage)==0):
            
            if isinstance(value["value"],bool):
                pass
            else:
                print(str(value) +" attribute expected a bool, but not a bool") 
                
                
        # only allow strings in string.
        elif (int(typestorage)==8):
            
            if isinstance(value["value"],str):
                pass
            else:
                print(str(value) +" attribute expected a string, but not a str") 